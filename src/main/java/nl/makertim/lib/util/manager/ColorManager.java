package nl.makertim.lib.util.manager;

import nl.makertim.lib.util.color.Colors;

/**
 * @author Tim Biesenbeek
 */
public class ColorManager extends Manager<Colors> {

	protected ColorManager() {
		super(Colors.class);
	}
}
