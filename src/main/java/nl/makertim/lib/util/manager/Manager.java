package nl.makertim.lib.util.manager;

import java.lang.reflect.Constructor;
import java.util.*;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

/**
 * @author Tim Biesenbeek
 */
public class Manager<T> {

	static {
		classes = new ArrayList<>();
		instance = new Manager<>(Manager.class);
		registerManagers("nl.makertim");
	}

	private static final List<Reflections> classes;
	@Getter
	private static final Manager<Manager> instance;

	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	protected final Map<Class<? extends T>, T> singletons = new HashMap<>();
	private Class<T> managing;

	protected Manager(Class<T> tType) {
		this.managing = tType;
		findSubtypes(LOGGER, singletons, tType);
	}

	private static <T> void findSubtypes(Logger logger, Map<Class<? extends T>, T> singletons, Class<T> tType) {
		classes.forEach(classes -> {
			findSubtypesIn(classes, logger, singletons, tType);
		});
	}

	private static <T> void findSubtypesIn(Reflections classes, Logger logger, Map<Class<? extends T>, T> singletons, Class<T> tType) {
		try {
			Collection<Class<? extends T>> tClasses = classes.getSubTypesOf(tType);
			tClasses.forEach(tClass -> {
				if (singletons.containsKey(tClass)) {
					return;
				}
				Constructor<? extends T> tConstructor = null;
				try {
					tConstructor = tClass.getDeclaredConstructor();
				} catch (Exception ex) {
					try {
						tConstructor = tClass.getConstructor();
					} catch (Exception ex2) {
						logger.error(tClass.getName() + " has no constructor to access");
						return;
					}
				}
				try {
					boolean accessible = tConstructor.isAccessible();
					tConstructor.setAccessible(true);
					T t = tConstructor.newInstance();
					singletons.put(tClass, t);
					tConstructor.setAccessible(accessible);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (Exception ex) {
			logger.error("Something went wrong with registering DAOs", ex);
		}
	}

	public static void registerManagers(String... classPaths) {
		for (String classPath : classPaths) {
			Reflections reflection = new Reflections(classPath);
			findSubtypesIn(reflection, getInstance().LOGGER, getInstance().singletons, Manager.class);

			getInstance().singletons.forEach((cls, manager) -> {
				findSubtypesIn(reflection, manager.LOGGER, manager.singletons, manager.managing);
			});
			classes.add(reflection);
		}
	}

	@SuppressWarnings("unchecked")
	public <R extends T> R getInstance(Class<R> cls) {
		return (R) singletons.get(cls);
	}

}
