package nl.makertim.lib.util.manager;

import nl.makertim.lib.util.database.DatabaseAccessObject;

/**
 * @author Tim Biesenbeek
 */
public class DAOManager extends Manager<DatabaseAccessObject> {

	protected DAOManager() {
		super(DatabaseAccessObject.class);
	}
}
