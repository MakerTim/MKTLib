package nl.makertim.lib.util.color;

import lombok.Getter;

public enum ChatColor {

	BLACK('0'),
	DARK_BLUE('1'),
	DARK_GREEN('2'),
	DARK_AQUA('3'),
	DARK_RED('4'),
	DARK_PURPLE('5'),
	GOLD('6'),
	GRAY('7'),
	DARK_GRAY('8'),
	BLUE('9'),
	GREEN('a'),
	AQUA('b'),
	RED('c'),
	LIGHT_PURPLE('d'),
	YELLOW('e'),
	WHITE('f'),
	MAGIC('k', true),
	BOLD('l', true),
	STRIKETHROUGH('m', true),
	UNDERLINE('n', true),
	ITALIC('o', true),
	RESET('r'),
	RAINBOW('z');

	public static final char DELIMITER = '&';

	@Getter
	private char key;
	@Getter
	private boolean isMarkup;

	ChatColor(char key) {
		this(key, false);
	}

	ChatColor(char key, boolean isMarkup) {
		this.key = key;
		this.isMarkup = isMarkup;
	}

	public boolean isReset() {
		return this == RESET;
	}

	@Override
	public String toString() {
		return String.valueOf(DELIMITER) + getKey();
	}
}
