package nl.makertim.lib.util.color;

import java.util.*;

/**
 * @author Tim Biesenbeek
 */
public class ColorParser {

	public static String parse(Colors colors, String in) {
		String[] splitText = splitter(colors, in);
		return fillWithTable(colors, splitText);
	}

	private static String fillWithTable(Colors colors, String[] in) {
		StringBuilder parsedText = new StringBuilder();
		StringBuilder closerTags = new StringBuilder();

		Map<Character, ColorTable> quickReference = colors.mapped();
		for (String coloredText : in) {
			if (coloredText.charAt(0) == ChatColor.DELIMITER && coloredText.length() > 1) {
				ColorTable table = quickReference.get(coloredText.charAt(1));
				if (table.getChatColor() == ChatColor.RAINBOW) {
					parsedText.append(rainbow(colors, coloredText.substring(2)));
					continue;
				}
				if (!table.getChatColor().isMarkup()) {
					parsedText.append(closerTags);
					closerTags = new StringBuilder();
				}
				parsedText.append(table.getPrefix());
				parsedText.append(coloredText.substring(2));
				if (!table.getChatColor().isMarkup()) {
					parsedText.append(table.getSuffix());
				} else {
					closerTags.append(table.getSuffix());
				}
			} else {
				parsedText.append(coloredText);
			}
		}

		parsedText.append(closerTags);
		return parsedText.toString();
	}

	private static String rainbow(Colors colors, String in) {
		ColorTable[] table = colors.getTable();
		List<Character> chatChars = new ArrayList<>(table.length - 1);

		// Give every character a chatcolor
		char[] inChars = in.toCharArray();
		String[] chars = new String[inChars.length];
		for (ColorTable colorTable : table) {
			ChatColor chatColor = colorTable.getChatColor();
			if (chatColor == ChatColor.RAINBOW) {
				continue;
			}
			chatChars.add(chatColor.getKey());
		}
		int colorSize = chatChars.size();
		for (int i = 0; i < inChars.length; i++) {
			chars[i] = String.valueOf(ChatColor.DELIMITER) + chatChars.get(i % colorSize) + inChars[i];
		}

		return fillWithTable(colors, chars);
	}

	private static String[] splitter(Colors colors, String in) {
		char[] keys = determainKeys(colors);
		Set<Integer> indexes = indexesOf(keys, in);

		List<String> splits = split(in, indexes.toArray(new Integer[indexes.size()]));
		return splits.toArray(new String[splits.size()]);
	}

	private static List<String> split(String in, Integer[] indexes) {
		List<String> splits = new LinkedList<>();
		int lastIndex = 0;
		for (int i = 0; i < indexes.length; i++) {
			int last = i - 1;
			lastIndex = indexes[i];
			if (last <= 0) {
				splits.add(in.substring(0, lastIndex));
			} else {
				splits.add(in.substring(indexes[last], lastIndex));
			}
		}
		if (lastIndex != in.length()) {
			splits.add(in.substring(lastIndex, in.length()));
		}

		splits.removeIf(String::isEmpty);
		return splits;
	}

	private static Set<Integer> indexesOf(char[] keys, String in) {
		SortedSet<Integer> indexesOf = new TreeSet<>(Integer::compare);
		for (char key : keys) {
			int lastIndex = 0;
			do {
				lastIndex = in.indexOf(String.valueOf(ChatColor.DELIMITER) + key, lastIndex);
				indexesOf.add(lastIndex);
				if (lastIndex != -1) {
					lastIndex++;
				}
			} while (lastIndex != -1);
		}
		indexesOf.remove(-1);
		return indexesOf;
	}

	private static char[] determainKeys(Colors colors) {
		ColorTable[] table = colors.getTable();
		char[] keys = new char[table.length];
		for (int i = 0; i < table.length; i++) {
			keys[i] = table[i].getChatColor().getKey();
		}
		return keys;
	}

}
