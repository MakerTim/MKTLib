package nl.makertim.lib.util.color;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import nl.makertim.lib.util.manager.ColorManager;
import nl.makertim.lib.util.manager.Manager;

/**
 * @author Tim Biesenbeek
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BukkitColors implements Colors {

	private static final String prefix = "\u00A7";

	private final static ColorTable[] colors = new ColorTable[]{ //
			new ColorTable(ChatColor.BLACK, prefix + "0", ""), //
			new ColorTable(ChatColor.DARK_BLUE, prefix + "1", ""), //
			new ColorTable(ChatColor.DARK_GREEN, prefix + "2", ""), //
			new ColorTable(ChatColor.DARK_AQUA, prefix + "3", ""), //
			new ColorTable(ChatColor.DARK_RED, prefix + "4", ""), //
			new ColorTable(ChatColor.DARK_PURPLE, prefix + "5", ""), //
			new ColorTable(ChatColor.GOLD, prefix + "6", ""), //
			new ColorTable(ChatColor.GRAY, prefix + "7", ""), //
			new ColorTable(ChatColor.DARK_GRAY, prefix + "8", ""), //
			new ColorTable(ChatColor.BLUE, prefix + "9", ""), //
			new ColorTable(ChatColor.GREEN, prefix + "a", ""), //
			new ColorTable(ChatColor.AQUA, prefix + "b", ""), //
			new ColorTable(ChatColor.RED, prefix + "c", ""), //
			new ColorTable(ChatColor.LIGHT_PURPLE, prefix + "d", ""), //
			new ColorTable(ChatColor.YELLOW, prefix + "e", ""), //
			new ColorTable(ChatColor.WHITE, prefix + "f", ""), //
			new ColorTable(ChatColor.MAGIC, prefix + "m", ""), //
			new ColorTable(ChatColor.BOLD, prefix + "l", ""), //
			new ColorTable(ChatColor.STRIKETHROUGH, prefix + "m", ""), //
			new ColorTable(ChatColor.UNDERLINE, prefix + "n", ""), //
			new ColorTable(ChatColor.ITALIC, prefix + "o", ""), //
			new ColorTable(ChatColor.RESET, prefix + "r", ""), //
			new ColorTable(ChatColor.RAINBOW, null, null) //
	};

	@Override
	public ColorTable[] getTable() {
		return colors;
	}

	public static BukkitColors getInstance() {
		return Manager.getInstance().getInstance(ColorManager.class).getInstance(BukkitColors.class);
	}
}
