package nl.makertim.lib.util.color;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import nl.makertim.lib.util.manager.ColorManager;
import nl.makertim.lib.util.manager.Manager;

/**
 * @author Tim Biesenbeek
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoColors implements Colors {

	private static final String prefix = "\u001B[";

	private final static ColorTable[] colors = new ColorTable[]{ //
			new ColorTable(ChatColor.BLACK, "", ""), //
			new ColorTable(ChatColor.DARK_BLUE, "", ""), //
			new ColorTable(ChatColor.DARK_GREEN, "", ""), //
			new ColorTable(ChatColor.DARK_AQUA, "", ""), //
			new ColorTable(ChatColor.DARK_RED, "", ""), //
			new ColorTable(ChatColor.DARK_PURPLE, "", ""), //
			new ColorTable(ChatColor.GOLD, "", ""), //
			new ColorTable(ChatColor.GRAY, "", ""), //
			new ColorTable(ChatColor.DARK_GRAY, "", ""), //
			new ColorTable(ChatColor.BLUE, "", ""), //
			new ColorTable(ChatColor.GREEN, "", ""), //
			new ColorTable(ChatColor.AQUA, "", ""), //
			new ColorTable(ChatColor.RED, "", ""), //
			new ColorTable(ChatColor.LIGHT_PURPLE, "", ""), //
			new ColorTable(ChatColor.YELLOW, "", ""), //
			new ColorTable(ChatColor.WHITE, "", ""), //
			new ColorTable(ChatColor.MAGIC, "", ""), //
			new ColorTable(ChatColor.BOLD, "", ""), //
			new ColorTable(ChatColor.STRIKETHROUGH, "", ""), //
			new ColorTable(ChatColor.UNDERLINE, "", ""), //
			new ColorTable(ChatColor.ITALIC, "", ""), //
			new ColorTable(ChatColor.RESET, "", ""), //
			new ColorTable(ChatColor.RAINBOW, null, null) //
	};

	@Override
	public ColorTable[] getTable() {
		return colors;
	}

	public static NoColors getInstance() {
		return Manager.getInstance().getInstance(ColorManager.class).getInstance(NoColors.class);
	}

}
