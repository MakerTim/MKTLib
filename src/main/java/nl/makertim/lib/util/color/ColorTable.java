package nl.makertim.lib.util.color;

import lombok.Data;

/**
 * @author Tim Biesenbeek
 */
public @Data class ColorTable {

	private final ChatColor chatColor;
	private final String prefix;
	private final String suffix;

	/*
	 * private final static ColorTable[] colors = new ColorTable[]{ // new
	 * ColorTable(ChatColor.BLACK, "", ""), // new ColorTable(ChatColor.DARK_BLUE,
	 * "", ""), // new ColorTable(ChatColor.DARK_GREEN, "", ""), // new
	 * ColorTable(ChatColor.DARK_AQUA, "", ""), // new
	 * ColorTable(ChatColor.DARK_RED, "", ""), // new
	 * ColorTable(ChatColor.DARK_PURPLE, "", ""), // new ColorTable(ChatColor.GOLD,
	 * "", ""), // new ColorTable(ChatColor.GRAY, "", ""), // new
	 * ColorTable(ChatColor.DARK_GRAY, "", ""), // new ColorTable(ChatColor.BLUE,
	 * "", ""), // new ColorTable(ChatColor.GREEN, "", ""), // new
	 * ColorTable(ChatColor.AQUA, "", ""), // new ColorTable(ChatColor.RED, "", ""),
	 * // new ColorTable(ChatColor.LIGHT_PURPLE, "", ""), // new
	 * ColorTable(ChatColor.YELLOW, "", ""), // new ColorTable(ChatColor.WHITE, "",
	 * ""), // new ColorTable(ChatColor.MAGIC, "", ""), // new
	 * ColorTable(ChatColor.BOLD, "", ""), // new
	 * ColorTable(ChatColor.STRIKETHROUGH, "", ""), // new
	 * ColorTable(ChatColor.UNDERLINE, "", ""), // new ColorTable(ChatColor.ITALIC,
	 * "", ""), // new ColorTable(ChatColor.RESET, "", ""), // new
	 * ColorTable(ChatColor.RAINBOW, "", "") // };
	 */

}
