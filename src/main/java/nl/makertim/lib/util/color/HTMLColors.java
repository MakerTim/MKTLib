package nl.makertim.lib.util.color;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import nl.makertim.lib.util.manager.ColorManager;
import nl.makertim.lib.util.manager.Manager;

/**
 * @author Tim Biesenbeek
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HTMLColors implements Colors {

	private final static ColorTable[] colors = new ColorTable[]{ //
			new ColorTable(ChatColor.BLACK, "<span style=\"color:black;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_BLUE, "<span style=\"color:darkblue;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_GREEN, "<span style=\"color:darkgreen;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_AQUA, "<span style=\"color:darkcyan;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_RED, "<span style=\"color:darkred;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_PURPLE, "<span style=\"color:purple;\">", "</span>"), //
			new ColorTable(ChatColor.GOLD, "<span style=\"color:gold;\">", "</span>"), //
			new ColorTable(ChatColor.GRAY, "<span style=\"color:gray;\">", "</span>"), //
			new ColorTable(ChatColor.DARK_GRAY, "<span style=\"color:darkgray;\">", "</span>"), //
			new ColorTable(ChatColor.BLUE, "<span style=\"color:blue;\">", "</span>"), //
			new ColorTable(ChatColor.GREEN, "<span style=\"color:green;\">", "</span>"), //
			new ColorTable(ChatColor.AQUA, "<span style=\"color:aqua;\">", "</span>"), //
			new ColorTable(ChatColor.RED, "<span style=\"color:red;\">", "</span>"), //
			new ColorTable(ChatColor.LIGHT_PURPLE, "<span style=\"color:pink;\">", "</span>"), //
			new ColorTable(ChatColor.YELLOW, "<span style=\"color:yellow;\">", "</span>"), //
			new ColorTable(ChatColor.WHITE, "<span style=\"color:white;\">", "</span>"), //
			new ColorTable(ChatColor.MAGIC, "<span style=\"font-family: wingdings;\">", "</span>"), //
			new ColorTable(ChatColor.BOLD, "<b>", "</b>"), //
			new ColorTable(ChatColor.STRIKETHROUGH, "<span style=\"text-decoration: line-through;\">", "</span>"), //
			new ColorTable(ChatColor.UNDERLINE, "<span style=\"text-decoration: underline;\">", "</span>"), //
			new ColorTable(ChatColor.ITALIC, "<span style=\"font-style: italic;\">", "</span>"), //
			new ColorTable(ChatColor.RESET, "<span>", "</span>"), //
			new ColorTable(ChatColor.RAINBOW, null, null) //
	};

	@Override
	public ColorTable[] getTable() {
		return colors;
	}

	public static HTMLColors getInstance() {
		return Manager.getInstance().getInstance(ColorManager.class).getInstance(HTMLColors.class);
	}
}
