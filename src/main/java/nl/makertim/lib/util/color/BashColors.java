package nl.makertim.lib.util.color;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import nl.makertim.lib.util.manager.ColorManager;
import nl.makertim.lib.util.manager.Manager;

/**
 * @author Tim Biesenbeek
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BashColors implements Colors {

	private static final String prefix = "\u001B[";

	private final static ColorTable[] colors = new ColorTable[]{ //
			new ColorTable(ChatColor.BLACK, prefix + "30m", ""), //
			new ColorTable(ChatColor.DARK_BLUE, prefix + "34m", ""), //
			new ColorTable(ChatColor.DARK_GREEN, prefix + "32m", ""), //
			new ColorTable(ChatColor.DARK_AQUA, prefix + "36m", ""), //
			new ColorTable(ChatColor.DARK_RED, prefix + "31m", ""), //
			new ColorTable(ChatColor.DARK_PURPLE, prefix + "35m", ""), //
			new ColorTable(ChatColor.GOLD, prefix + "33m", ""), //
			new ColorTable(ChatColor.GRAY, prefix + "30m", ""), //
			new ColorTable(ChatColor.DARK_GRAY, prefix + "30m", ""), //
			new ColorTable(ChatColor.BLUE, prefix + "34m", ""), //
			new ColorTable(ChatColor.GREEN, prefix + "32m", ""), //
			new ColorTable(ChatColor.AQUA, prefix + "36m", ""), //
			new ColorTable(ChatColor.RED, prefix + "31m", ""), //
			new ColorTable(ChatColor.LIGHT_PURPLE, prefix + "35m", ""), //
			new ColorTable(ChatColor.YELLOW, prefix + "33m", ""), //
			new ColorTable(ChatColor.WHITE, prefix + "37m", ""), //
			new ColorTable(ChatColor.MAGIC, prefix + "47m", ""), //
			new ColorTable(ChatColor.BOLD, prefix + "1m", ""), //
			new ColorTable(ChatColor.STRIKETHROUGH, prefix + "2m", ""), //
			new ColorTable(ChatColor.UNDERLINE, prefix + "4m", ""), //
			new ColorTable(ChatColor.ITALIC, prefix + "3m", ""), //
			new ColorTable(ChatColor.RESET, prefix + "0m", ""), //
			new ColorTable(ChatColor.RAINBOW, null, null) //
	};

	@Override
	public ColorTable[] getTable() {
		return colors;
	}

	public static BashColors getInstance() {
		return Manager.getInstance().getInstance(ColorManager.class).getInstance(BashColors.class);
	}

}
