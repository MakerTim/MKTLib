package nl.makertim.lib.util.color;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tim Biesenbeek
 */
public interface Colors {

	ColorTable[] getTable();

	default Map<Character, ColorTable> mapped() {
		ColorTable[] table = getTable();

		Map<Character, ColorTable> map = new HashMap<>();
		for (ColorTable colorTable : table) {
			map.put(colorTable.getChatColor().getKey(), colorTable);
		}
		return map;
	}
}
