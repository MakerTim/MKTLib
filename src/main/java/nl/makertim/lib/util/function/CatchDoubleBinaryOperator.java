package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleBinaryOperator<Thr extends Throwable> {

	double applyAsDouble(double left, double right) throws Thr;
}