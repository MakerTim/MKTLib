package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchBooleanSupplier<Thr extends Throwable> {

	boolean getAsBoolean() throws Thr;

}
