package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchConsumer<T, Thr extends Throwable> {

	void accept(T t) throws Thr;

	default CatchConsumer<T, Thr> andThen(CatchConsumer<? super T, ? extends Thr> after) throws Thr {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
