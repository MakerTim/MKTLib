package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntUnaryOperator<Thr extends Throwable> {

	int applyAsInt(int operand) throws Thr;

	default CatchIntUnaryOperator<Thr> compose(CatchIntUnaryOperator<Thr> before) {
		return (int v) -> applyAsInt(before.applyAsInt(v));
	}

	default CatchIntUnaryOperator<Thr> andThen(CatchIntUnaryOperator<Thr> after) {
		return (int t) -> after.applyAsInt(applyAsInt(t));
	}

	static <Thr extends Throwable> CatchIntUnaryOperator<Thr> identity() {
		return t -> t;
	}
}
