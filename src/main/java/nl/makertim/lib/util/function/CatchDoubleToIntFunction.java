package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleToIntFunction<Thr extends Throwable> {

	int applyAsInt(double value) throws Thr;
}
