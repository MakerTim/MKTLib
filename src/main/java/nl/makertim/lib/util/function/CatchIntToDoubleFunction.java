package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntToDoubleFunction<Thr extends Throwable> {

	double applyAsDouble(int value) throws Thr;
}
