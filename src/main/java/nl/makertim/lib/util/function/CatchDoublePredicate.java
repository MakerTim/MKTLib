package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoublePredicate<Thr extends Throwable> {

	boolean test(double value) throws Thr;

	default CatchDoublePredicate<Thr> and(CatchDoublePredicate<Thr> other) {
		return (value) -> test(value) && other.test(value);
	}

	default CatchDoublePredicate<Thr> negate() {
		return (value) -> !test(value);
	}

	default CatchDoublePredicate<Thr> or(CatchDoublePredicate<Thr> other) {
		return (value) -> test(value) || other.test(value);
	}
}
