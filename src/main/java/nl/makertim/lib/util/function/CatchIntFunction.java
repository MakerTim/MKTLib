package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntFunction<R, Thr extends Throwable> {

	R apply(int var1) throws Thr;
}