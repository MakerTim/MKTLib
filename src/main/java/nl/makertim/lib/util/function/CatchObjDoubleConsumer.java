package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchObjDoubleConsumer<T, Thr extends Throwable> {

	void accept(T t, double value) throws Thr;
}
