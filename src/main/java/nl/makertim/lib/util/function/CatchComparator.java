package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchComparator<T, Thr extends Throwable> {

	int compare(T var1, T var2) throws Thr;
}