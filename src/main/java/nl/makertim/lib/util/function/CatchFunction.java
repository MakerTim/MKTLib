package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchFunction<T, R, Thr extends Throwable> {

	R apply(T t) throws Thr;

	default <V> CatchFunction<V, R, Thr> compose(CatchFunction<? super V, ? extends T, ? extends Thr> before) {
		return (V v) -> apply(before.apply(v));
	}

	default <V> CatchFunction<T, V, Thr> andThen(CatchFunction<? super R, ? extends V, ? extends Thr> after) {
		return (T t) -> after.apply(apply(t));
	}

	static <T, Thr extends Throwable> CatchFunction<T, T, ? extends Thr> identity() {
		return t -> t;
	}
}
