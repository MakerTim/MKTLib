package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntSupplier<Thr extends Throwable> {

	int getAsInt() throws Thr;
}
