package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToLongFunction<T, Thr extends Throwable> {

	long applyAsLong(T value) throws Thr;
}