package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleToLongFunction<Thr extends Throwable> {

	long applyAsLong(double value) throws Thr;
}
