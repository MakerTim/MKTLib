package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntPredicate<Thr extends Throwable> {

	boolean test(int value) throws Thr;

	default CatchIntPredicate<Thr> and(CatchIntPredicate<Thr> other) {
		return (value) -> test(value) && other.test(value);
	}

	default CatchIntPredicate<Thr> negate() {
		return (value) -> !test(value);
	}

	default CatchIntPredicate<Thr> or(CatchIntPredicate<Thr> other) {
		return (value) -> test(value) || other.test(value);
	}
}
