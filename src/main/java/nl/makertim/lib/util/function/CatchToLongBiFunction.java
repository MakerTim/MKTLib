package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToLongBiFunction<T, U, Thr extends Throwable> {

	long applyAsLong(T t, U u) throws Thr;
}