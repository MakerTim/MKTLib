package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntBinaryOperator<Thr extends Throwable> {

	int applyAsInt(int left, int right) throws Thr;
}
