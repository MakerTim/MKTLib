package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToIntFunction<T, Thr extends Throwable> {

	int applyAsInt(T value) throws Thr;
}
