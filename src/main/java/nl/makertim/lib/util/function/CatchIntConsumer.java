package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntConsumer<Thr extends Throwable> {

	void accept(int value) throws Thr;

	default CatchIntConsumer<Thr> andThen(CatchIntConsumer<Thr> after) {
		return (int t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
