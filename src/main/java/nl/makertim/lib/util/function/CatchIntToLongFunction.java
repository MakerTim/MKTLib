package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchIntToLongFunction<Thr extends Throwable> {

	long applyAsLong(int value) throws Thr;
}
