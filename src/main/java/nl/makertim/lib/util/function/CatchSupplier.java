package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchSupplier<T, Thr extends Throwable> {

	T get() throws Thr;
}
