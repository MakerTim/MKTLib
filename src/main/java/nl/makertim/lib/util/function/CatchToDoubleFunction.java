package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToDoubleFunction<T, Thr extends Throwable> {

	double applyAsDouble(T value) throws Thr;
}