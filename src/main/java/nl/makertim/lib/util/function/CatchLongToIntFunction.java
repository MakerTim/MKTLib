package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongToIntFunction<Thr extends Throwable> {

	int applyAsInt(long value) throws Thr;
}
