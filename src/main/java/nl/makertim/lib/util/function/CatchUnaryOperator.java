package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchUnaryOperator<T, Thr extends Throwable> extends CatchFunction<T, T, Thr> {

	static <T, Thr extends Throwable> CatchUnaryOperator<T, Thr> identity() {
		return t -> t;
	}
}