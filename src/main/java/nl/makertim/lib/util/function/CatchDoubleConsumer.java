package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleConsumer<Thr extends Throwable> {

	void accept(double value) throws Thr;

	default CatchDoubleConsumer<Thr> andThen(CatchDoubleConsumer<? extends Thr> after) {
		return (double t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
