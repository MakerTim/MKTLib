package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongToDoubleFunction<Thr extends Throwable> {

	double applyAsDouble(long value) throws Thr;
}
