package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongConsumer<Thr extends Throwable> {

	void accept(long value) throws Thr;

	default CatchLongConsumer<Thr> andThen(CatchLongConsumer<? extends Thr> after) {
		return (long t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
