package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToIntBiFunction<T, U, Thr extends Throwable> {

	int applyAsInt(T t, U u) throws Thr;
}