package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongSupplier<Thr extends Throwable> {

	long getAsLong() throws Thr;
}