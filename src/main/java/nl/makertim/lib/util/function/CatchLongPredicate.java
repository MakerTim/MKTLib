package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongPredicate<Thr extends Throwable> {

	boolean test(long value) throws Thr;

	default CatchLongPredicate<Thr> and(CatchLongPredicate<? extends Thr> other) {
		return (value) -> test(value) && other.test(value);
	}

	default CatchLongPredicate<Thr> negate() {
		return (value) -> !test(value);
	}

	default CatchLongPredicate<Thr> or(CatchLongPredicate<? extends Thr> other) {
		return (value) -> test(value) || other.test(value);
	}
}
