package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchBiConsumer<T, U, Thr extends Throwable> {

	void accept(T t) throws Thr;

	default CatchBiConsumer<T, U, Thr> andThen(CatchBiConsumer<? super T, ? super U, ? extends Thr> after) throws Thr {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
