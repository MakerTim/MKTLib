package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongUnaryOperator<Thr extends Throwable> {

	long applyAsLong(long operand) throws Thr;

	default CatchLongUnaryOperator<Thr> compose(CatchLongUnaryOperator<? extends Thr> before) {
		return (long v) -> applyAsLong(before.applyAsLong(v));
	}

	default CatchLongUnaryOperator<Thr> andThen(CatchLongUnaryOperator<? extends Thr> after) {
		return (long t) -> after.applyAsLong(applyAsLong(t));
	}

	static <Thr extends Throwable> CatchLongUnaryOperator<Thr> identity() {
		return t -> t;
	}
}
