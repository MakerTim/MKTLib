package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleUnaryOperator<Thr extends Throwable> {

	double applyAsDouble(double operand) throws Thr;

	default CatchDoubleUnaryOperator<Thr> compose(CatchDoubleUnaryOperator<Thr> before) {
		return (double v) -> applyAsDouble(before.applyAsDouble(v));
	}

	default CatchDoubleUnaryOperator<Thr> andThen(CatchDoubleUnaryOperator<Thr> after) {
		return (double t) -> after.applyAsDouble(applyAsDouble(t));
	}

	static <Thr extends Throwable> CatchDoubleUnaryOperator<Thr> identity() {
		return t -> t;
	}
}
