package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchBiFunction<T, U, R, Thr extends Throwable> {

	R apply(T var1, U var2) throws Thr;

	default <V> CatchBiFunction<T, U, V, Thr> andThen(CatchFunction<? super R, ? extends V, ? extends Thr> var1) {
		return (var2, var3) -> {
			return var1.apply(this.apply(var2, var3));
		};
	}
}
