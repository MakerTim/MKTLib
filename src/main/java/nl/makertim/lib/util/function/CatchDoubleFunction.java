package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleFunction<R, Thr extends Throwable> {

	R apply(double value) throws Thr;
}