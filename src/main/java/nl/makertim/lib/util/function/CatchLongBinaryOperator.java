package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongBinaryOperator<Thr extends Throwable> {

	long applyAsLong(long left, long right) throws Thr;
}
