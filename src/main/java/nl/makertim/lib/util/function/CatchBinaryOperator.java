package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchBinaryOperator<T, Thr extends Throwable> extends CatchBiFunction<T, T, T, Thr> {

	static <T, Thr extends Throwable> CatchBinaryOperator<T, Thr> minBy(CatchComparator<? super T, ? extends Thr> comparator) {
		return (a, b) -> comparator.compare(a, b) <= 0 ? a : b;
	}

	static <T, Thr extends Throwable> CatchBinaryOperator<T, Thr> maxBy(CatchComparator<? super T, ? extends Thr> comparator) {
		return (a, b) -> comparator.compare(a, b) >= 0 ? a : b;
	}
}