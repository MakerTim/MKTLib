package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchObjIntConsumer<T, Thr extends Throwable> {

	void accept(T t, int value) throws Thr;
}
