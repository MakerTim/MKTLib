package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchObjLongConsumer<T, Thr extends Throwable> {

	void accept(T t, long value) throws Thr;
}