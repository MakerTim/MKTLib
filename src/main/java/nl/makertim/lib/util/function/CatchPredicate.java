package nl.makertim.lib.util.function;

import java.util.Objects;

@FunctionalInterface
public interface CatchPredicate<T, Thr extends Throwable> {

	boolean test(T t) throws Thr;

	default CatchPredicate<T, Thr> and(CatchPredicate<? super T, ? extends Thr> other) {
		return (t) -> test(t) && other.test(t);
	}

	default CatchPredicate<T, Thr> negate() {
		return (t) -> !test(t);
	}

	default CatchPredicate<T, Thr> or(CatchPredicate<? super T, ? extends Thr> other) {
		return (t) -> test(t) || other.test(t);
	}

	static <T, Thr extends Throwable> CatchPredicate<T, Thr> isEqual(Object targetRef) {
		return (null == targetRef) ? Objects::isNull : targetRef::equals;
	}
}
