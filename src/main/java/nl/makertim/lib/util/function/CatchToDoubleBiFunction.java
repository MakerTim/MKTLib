package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchToDoubleBiFunction<T, U, Thr extends Throwable> {

	double applyAsDouble(T t, U u) throws Thr;
}
