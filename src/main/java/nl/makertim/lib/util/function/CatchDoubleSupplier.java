package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchDoubleSupplier<Thr extends Throwable> {

	double getAsDouble() throws Thr;
}
