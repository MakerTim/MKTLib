package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchLongFunction<R, Thr extends Throwable> {

	R apply(long value) throws Thr;
}
