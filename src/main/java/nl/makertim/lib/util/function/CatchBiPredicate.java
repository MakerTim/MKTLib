package nl.makertim.lib.util.function;

@FunctionalInterface
public interface CatchBiPredicate<T, U, Thr extends Throwable> {

	boolean test(T t, U u) throws Thr;

	default CatchBiPredicate<T, U, Thr> and(CatchBiPredicate<? super T, ? super U, ? extends Thr> other) {
		return (T t, U u) -> test(t, u) && other.test(t, u);
	}

	default CatchBiPredicate<T, U, Thr> negate() {
		return (T t, U u) -> !test(t, u);
	}

	default CatchBiPredicate<T, U, Thr> or(CatchBiPredicate<? super T, ? super U, ? extends Thr> other) {
		return (T t, U u) -> test(t, u) || other.test(t, u);
	}
}
