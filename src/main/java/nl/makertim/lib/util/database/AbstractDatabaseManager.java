package nl.makertim.lib.util.database;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Getter;
import nl.makertim.lib.util.function.CatchConsumer;

/**
 * @author Tim Biesenbeek
 */
public abstract class AbstractDatabaseManager extends DataStorageManager implements Runnable {

	/**
	 * Conection with the database
	 */
	@Getter
	protected Connection connection;
	private int delay;
	private int port;
	private final String type;
	private final String username;
	private final String password;
	private final String database;
	private final String url;
	private final String fullUrl;

	/**
	 * This is a wrapper for databases
	 *
	 * @param type
	 *            the type of the server, like 'mysql'
	 * @param url
	 *            the url/ip of the server
	 * @param username
	 *            the username to login with
	 * @param password
	 *            the password to login with
	 * @param database
	 *            the name of the database
	 */
	public AbstractDatabaseManager(String type, String url, int port, String username, String password, String database) {
		this.port = port;
		this.type = type;
		this.database = database;
		this.username = username;
		this.password = password;
		this.url = url;
		this.fullUrl = String.format("jdbc:%s://%s:%d/%s", type, url, port, database);
		new Thread(this).start();
	}

	public AbstractDatabaseManager(String jdbcUrl, String username, String password) {
		String regex = "^jdbc:(.+):[/?]+(.+?)(?::(\\d+))?/(.+)(?:\\?.+)?$";
		Matcher matcher = Pattern.compile(regex).matcher(jdbcUrl);
		if (matcher.find()) {
			this.type = matcher.group(1);
			this.url = matcher.group(2);
			if (matcher.groupCount() == 5 && matcher.group(3) != null) {
				this.port = Integer.parseInt(matcher.group(3));
			}
			this.database = matcher.group(4);
		} else {
			throw new IllegalArgumentException(jdbcUrl);
		}
		this.username = username;
		this.password = password;
		this.fullUrl = jdbcUrl;
		new Thread(this).start();
	}

	@Override
	public Class<? extends Connection> getConnectionClass() {
		return connection.getClass();
	}

	@Override
	public String getRawVersion() {
		try {
			return connection.getMetaData().getDriverVersion();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return "-1";
	}

	@Override
	public boolean openConnection() {
		boolean didOpen = false;
		try {
			connection = DriverManager.getConnection(fullUrl, username, password);
			didOpen = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return didOpen;
	}

	@Override
	public boolean closeConnection() {
		boolean didClose = false;
		try {
			if (connection != null) {
				connection.close();
				didClose = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			connection = null;
		}
		return didClose;
	}

	@Override
	public ResultSet executePrepared(String query, CatchConsumer<PreparedStatement, SQLException> prepare) {
		try {
			openIfNotClosed();
			PreparedStatement statement = connection.prepareStatement(query);
			prepare.accept(statement);
			return statement.executeQuery();
		} catch (Exception ex) {
			System.err.println(ex.toString());
			return null;
		}
	}

	@Override
	public ResultSet updatePrepared(String query, CatchConsumer<PreparedStatement, SQLException> prepare) {
		try {
			openIfNotClosed();
			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			prepare.accept(statement);
			statement.executeUpdate();
			return statement.getGeneratedKeys();
		} catch (Exception ex) {
			System.err.println(ex.toString());
			return null;
		}
	}

	public PreparedStatement prepareStatement(String query) {
		try {
			openIfNotClosed();
			return connection.prepareStatement(query);
		} catch (Exception ex) {
			System.err.println(ex.toString());
			return null;
		}
	}

	@Override
	public boolean executeQuery(String query) {
		return selectQuery(query) != null;
	}

	@Override
	public ResultSet selectQuery(String query) {
		return executePrepared(query, preparedStatement -> {
		});
	}

	@Override
	public ResultSet insertQuery(String query) {
		return updatePrepared(query, preparedStatement -> {
		});
	}

	@Override
	public ResultSet updateQuery(String query) {
		return insertQuery(query);
	}

	@Override
	public boolean deleteQuery(String query) {
		try {
			insertQuery(query);
			return true;
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
		return false;
	}

	private void openIfNotClosed() throws SQLException {
		delay = 100;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT 1;");
			preparedStatement.executeQuery();
		} catch (SQLException | NullPointerException ex) {
			this.openConnection();
		}
	}

	@Override
	public void run() {
		delay--;
		if (delay == 0) {
			closeConnection();
		}
		try {
			Thread.sleep(1000L);
		} catch (Exception ignored) {
		}
	}
}
