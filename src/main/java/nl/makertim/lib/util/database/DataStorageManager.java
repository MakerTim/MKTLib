package nl.makertim.lib.util.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import nl.makertim.lib.util.function.CatchConsumer;

/**
 * @author Tim Biesenbeek
 */
public abstract class DataStorageManager {

	protected abstract Class<? extends Connection> getConnectionClass();

	/**
	 *
	 * Open a connection with the credentials
	 * 
	 * @return true, if succeed
	 */
	protected abstract boolean openConnection();

	/**
	 *
	 * Close connection if not already
	 * 
	 * @return true, if closed
	 */
	protected abstract boolean closeConnection();

	/**
	 * The version of the database
	 * 
	 * @return a number representing the version
	 */
	public double getVersion() {
		String ret = getRawVersion();
		if (ret == null) {
			return -1;
		}
		ret = ret.replaceFirst("\\.", "");
		try {
			return Double.parseDouble(ret);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return -1;
	}

	/**
	 * Gets the version of the database
	 * 
	 * @return a String containing a version
	 */
	public abstract String getRawVersion();

	/**
	 * Execute a prepared SQL, for getting data
	 * 
	 * @param query
	 *            the query that will be execute
	 * @param prepare
	 *            a consumer for inserting variables
	 * @return The entire result of the query
	 */
	public abstract ResultSet executePrepared(String query, CatchConsumer<PreparedStatement, SQLException> prepare);

	/**
	 * Execute a prepared SQL, for modifying data
	 *
	 * @param query
	 *            the query that will be execute
	 * @param prepare
	 *            a consumer for inserting variables
	 * @return Only the generated keys of the query
	 */
	public abstract ResultSet updatePrepared(String query, CatchConsumer<PreparedStatement, SQLException> prepare);

	/**
	 *
	 * Execute sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return true, if succeeded
	 */
	public boolean executeQuery(String query, Object... obj) {
		return executeQuery(String.format(query, obj));
	}

	/**
	 *
	 * Execute sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return true, if succeeded
	 */
	public abstract boolean executeQuery(String query);

	/**
	 * Gets the first object in a database
	 * 
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return The first field of the first object from the query
	 */
	public Object getFirst(String query, Object... obj) {
		return getFirst(String.format(query, obj));
	}

	/**
	 * Gets the first object in a database
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return The first field of the first object from the query
	 */
	public Object getFirst(String query) {
		ResultSet rs = selectQuery(query);
		Object ret = null;
		try {
			if (rs.next()) {
				ret = rs.getObject(1);
			}
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
		return ret;
	}

	/**
	 * Execute SELECT sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return The entire result of the query
	 */
	public ResultSet selectQuery(String query, Object... obj) {
		return selectQuery(String.format(query, obj));
	}

	/**
	 *
	 * Execute SELECT sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return The entire result of the query
	 */
	public abstract ResultSet selectQuery(String query);

	/**
	 *
	 * Execute INSERT sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return the keys that were generated, or empty ResultSet when non where
	 *         generated. null if fails.
	 */
	public ResultSet insertQuery(String query, Object... obj) {
		return insertQuery(String.format(query, obj));
	}

	/**
	 *
	 * Execute INSERT sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return the keys that were generated, or empty ResultSet when non where
	 *         generated. null if fails.
	 */
	public abstract ResultSet insertQuery(String query);

	/**
	 *
	 * Execute UPDATE sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return the rows that where updated.
	 */
	public ResultSet updateQuery(String query, Object... obj) {
		return updateQuery(String.format(query, obj));
	}

	/**
	 *
	 * Execute UPDATE sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return the rows that where updated.
	 */
	public abstract ResultSet updateQuery(String query);

	/**
	 *
	 * Execute DELETE sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @param obj
	 *            The objects that will be formatted into the query
	 * @return true if succeed
	 */
	public boolean deleteQuery(String query, Object... obj) {
		return deleteQuery(String.format(query, obj));
	}

	/**
	 *
	 * Execute DELETE sql
	 *
	 * @param query
	 *            The SQL that will be executed
	 * @return true if succeed
	 */
	public abstract boolean deleteQuery(String query);
}
