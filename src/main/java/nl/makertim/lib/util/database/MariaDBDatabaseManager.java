package nl.makertim.lib.util.database;

import java.sql.ResultSet;

/**
 * @author Tim Biesenbeek
 */
public class MariaDBDatabaseManager extends AbstractDatabaseManager {

	/**
	 * This is a wrapper for databases
	 *
	 * @param url
	 *            the url/ip of the server
	 * @param username
	 *            the username to login with
	 * @param password
	 *            the password to login with
	 * @param database
	 *            the name of the database
	 */
	public MariaDBDatabaseManager(String url, int port, String username, String password, String database) {
		super("mariadb", url, port, username, password, database);
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * This is a wrapper for databases
	 * 
	 * @param url
	 *            the url/ip of the server
	 * @param username
	 *            the username to login with
	 * @param password
	 *            the password to login with
	 * @param database
	 *            the name of the database
	 */
	public MariaDBDatabaseManager(String url, String username, String password, String database) {
		this(url, 3306, username, password, database);
	}

	public MariaDBDatabaseManager(String jdbcUrl, String username, String password) {
		super(jdbcUrl, username, password);
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public String getRawVersion() {
		String ret = null;
		ResultSet rs = selectQuery("SELECT @@version as \"version\";");
		try {
			if (rs != null && rs.next()) {
				ret = rs.getString("version");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
}