package nl.makertim.lib.util.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import lombok.Cleanup;
import lombok.NonNull;
import nl.makertim.lib.util.function.CatchSupplier;

/**
 * @author Tim Biesenbeek.
 */
public abstract class DatabaseAccessObject<T extends Model> {

	private static AbstractDatabaseManager databaseManager;
	public static final int NO_LIMIT = -1;

	// SET ME PLZ
	public static void setDatabaseManager(AbstractDatabaseManager databaseManager) {
		DatabaseAccessObject.databaseManager = databaseManager;
	}

	protected AbstractDatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	protected abstract T result(ResultSet rs) throws SQLException;

	public abstract boolean create(T model);

	public List<T> read() {
		return read(NO_LIMIT);
	}

	public List<T> read(int limit) {
		List<T> models = new ArrayList<>();
		try {
			@Cleanup
			PreparedStatement select = select(limit);
			@Cleanup
			ResultSet rs = select.executeQuery();
			while (rs.next()) {
				T model = result(rs);
				if (model == null) {
					continue;
				}
				models.add(model);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return models;
	}

	public abstract boolean update(T model);

	public abstract boolean delete(T model);

	protected PreparedStatement insert() throws SQLException {
		StringBuilder columnString = new StringBuilder();
		StringBuilder vars = new StringBuilder();
		Iterator<String> columnIt = Arrays.asList(columns()).iterator();
		while (columnIt.hasNext()) {
			columnString.append('`').append(columnIt.next()).append('`');
			vars.append("?");
			if (columnIt.hasNext()) {
				columnString.append(", ");
				vars.append(", ");
			}
		}
		return getDatabaseManager()
				.prepareStatement(String.format("INSERT INTO %s (%s) VALUES (%s);", tableName(), columnString, vars));
	}

	protected T where(CatchSupplier<PreparedStatement, SQLException> where) {
		try {
			@Cleanup
			PreparedStatement select = where.get();
			@Cleanup
			ResultSet rs = select.executeQuery();

			if (rs.next()) {
				return result(rs);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	protected List<T> whereList(CatchSupplier<PreparedStatement, SQLException> where) {
		List<T> list = new ArrayList<>();
		try {
			@Cleanup
			PreparedStatement select = where.get();
			@Cleanup
			ResultSet rs = select.executeQuery();

			while (rs.next()) {
				list.add(result(rs));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return list;
	}

	protected PreparedStatement select(int limit) throws SQLException {
		return getDatabaseManager().prepareStatement(
			String.format("SELECT * FROM %s %s;", tableName(), limit != NO_LIMIT ? "LIMIT " + limit : ""));
	}

	protected PreparedStatement select(String where) throws SQLException {
		return select(where, NO_LIMIT);
	}

	protected PreparedStatement select(String where, int limit) throws SQLException {
		return getDatabaseManager().prepareStatement(String.format("SELECT * FROM %s WHERE %s %s;", tableName(), where,
			limit != NO_LIMIT ? "LIMIT " + limit : ""));
	}

	protected PreparedStatement update(String where) throws SQLException {
		StringBuilder columnString = new StringBuilder();
		Iterator<String> columnIt = Arrays.asList(columns()).iterator();
		while (columnIt.hasNext()) {
			columnString.append('`').append(columnIt.next()).append('`').append("=?");
			if (columnIt.hasNext()) {
				columnString.append(", ");
			}
		}
		return getDatabaseManager()
				.prepareStatement(String.format("UPDATE %s SET %s WHERE %s;", tableName(), columnString, where));
	}

	protected PreparedStatement delete(String where) throws SQLException {
		return getDatabaseManager().prepareStatement(String.format("DELETE FROM %s WHERE %s;", tableName(), where));
	}

	@NonNull
	protected abstract String tableName();

	@NonNull
	protected abstract String[] columns();
}
